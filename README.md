# Simple 3D Perspective & Rotation Demo

## Getting Started

This is a very simple 3D Perspective and rotation demo in Flutter.

I have placed notes in the code to allow you to play with the code
and experiment with various rotations and adjust the amount of perspective.

The demo allows you to rotate the image on the screen by dragging it. It 
will change it's perspective projection as it is rotated. You can easily
see the image perspective changing with the edge closest to the user getting 
larger and the edge farthest away from the user getting smaller.

This same code could be used in a game library for Flutter apps to adjust 
image perspectives during game play.

You should not use this code without a basic understanding of what the
flutter framework is doing for you or you may end up with weird results
and no understanding of why it happened.

If you need further information on 3D transformations check out the links below:

- [Computerphile: The Power of the Matrix](https://www.youtube.com/watch?v=vQ60rFwh2ig&t=5s)
- [Udacity: Interactive 3D Graphics](https://www.udacity.com/course/interactive-3d-graphics--cs291)
- [CCSU: Vector Math for 3D Computer Graphics](http://chortle.ccsu.edu/vectorlessons/vectorindex.html)
- [Lab: Write your first Flutter app](https://flutter.io/docs/get-started/codelab)
- [Medium: A Deep Dive Into Transform Widgets in Flutter](https://medium.com/flutter-community/a-deep-dive-into-transform-widgets-in-flutter-4dc32cd575a9)
- [Flutter.io: Matrix Math Library](https://docs.flutter.io/flutter/vector_math/vector_math-library.html)
- [Flutter.io: Transform Widget](https://docs.flutter.io/flutter/widgets/Transform-class.html)
- [Flutter.io: Animation Class](https://docs.flutter.io/flutter/animation/Animation-class.html)


For help getting started with Flutter, view the
[online documentation](https://flutter.io/docs), which offers tutorials, 
samples, guidance on mobile development, and a full API reference.

Here's some eye candy:

![picture](assets/Screenshot_01.png)
![picture](assets/Screenshot_02.png)
![picture](assets/Screenshot_03.png)
![picture](assets/Screenshot_04.png)
![picture](assets/Screenshot_05.png)
![picture](assets/Screenshot_06.png)
![picture](assets/Screenshot_07.png)
![picture](assets/Screenshot_08.png)
![picture](assets/Screenshot_09.png)
