import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Offset _offset = Offset.zero;

  @override
  Widget build(BuildContext context) {
    return _defaultApp(context);
  }

  Widget _defaultApp(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('3D Perspective Demo'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildImage(context),
          ],
        ),
      ),
    );
  }

  Widget _buildImage(BuildContext context) {
    return Transform(
      transform: Matrix4.identity()
        // Note: Setting too much perspective below
        // will cuase the image to stretch.
        // Too little and the image looks flat.
        // Rotation Matrix Below:
        // [[ 0, 0, 0, 0 ]
        //  [ 0, 0, 0, 0 ]
        //  [ 0, 0, 0, 0 ]
        //  [ 0, 0.001, 0, 0 ]]
        //
        ..setEntry(3, 2, 0.001) // Set row 3 column 2 to 0.001 for perspective
        ..rotateX(0.005 * _offset.dy) // Apply rotations
        ..rotateY(-0.005 * _offset.dx)
        ..rotateZ(0.0),
      // Note rotateX uses _offsetY and rotateY uses
      // _offsetX because we are passing the axis to
      // rotate around. Try removing rotation around
      // the X or Y axis and replace it with rotation
      // around the Z axis.
      alignment: FractionalOffset.center,
      child: GestureDetector(
        onPanUpdate: (details) => setState(() => _offset += details.delta),
        onDoubleTap: () => setState(() => _offset = Offset.zero),
        child: Image.asset('assets/artsy.jpg'),
      ),
    );
  }
}
